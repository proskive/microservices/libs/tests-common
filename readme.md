# ProSkive.Lib.TestsCommon

.NET Standard 2.0 Library including common code for ProSkive tests.

## Settings (Examples)

##### Use FakeJwt for testing

Copy the `Startup.cs` to a file like `TestStartup.cs` and add following snippet:

```csharp
services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = FakeJwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = FakeJwtBearerDefaults.AuthenticationScheme;
})
.AddFakeJwtBearer();
```

You can then use it in your test files (using NUnit and Flurl):

```csharp
[OneTimeSetUp]
public void Init()
{
    // Test Server, second argument is the name of the assembly
    server = TestServerHelper.CreateTestServer<TestStartup>(TestEnv.Get(), "ProSkive.Services.Notifications");

    // Flurl Factory Config
    FlurlTestHelper.UseInMemoryServer(server);
}
```

## License GPL2

```
ProSkive
Copyright (C) 2018 Medical Informatics Group (MIG) Frankfurt

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
USA.
```