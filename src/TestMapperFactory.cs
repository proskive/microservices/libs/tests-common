﻿using AutoMapper;

namespace ProSkive.Lib.TestsCommon
{
    public static class TestMapperFactory
    {
        public static IMapper GetMapper<TMappingProfile>() where TMappingProfile : Profile, new()
        {
            var config = new MapperConfiguration(cfg => cfg.AddProfile(new TMappingProfile()));
            return config.CreateMapper();
        }
    }
}