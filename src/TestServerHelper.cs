﻿using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;

namespace ProSkive.Lib.TestsCommon
{
    public static class TestServerHelper
    {
        public static TestServer CreateTestServer<TStartup>(string environment, string subfolder = null) where TStartup : class
        {
            return new TestServer(new WebHostBuilder()
                .UseKestrel()
                .UseEnvironment(environment)
                .ConfigureAppConfiguration((context, config) =>
                {
                    var env = context.HostingEnvironment;

                    var path = Directory.GetCurrentDirectory();
                    var depthLimit = subfolder != null ? 5 : 4;
                    for (var i = 0; i < depthLimit; ++i)
                    {
                        path = Directory.GetParent(path).FullName;
                    }

                    path = subfolder != null ? Path.Combine(path, "src", subfolder) : Path.Combine(path, "src");
                    
                    config.AddJsonFile(Path.Combine(path, "appsettings.json"), optional: true, reloadOnChange: true)
                        .AddJsonFile(Path.Combine(path, $"appsettings.{env.EnvironmentName}.json"));

                    config.AddEnvironmentVariables();
                })
                .UseStartup<TStartup>());
        }
    }
}