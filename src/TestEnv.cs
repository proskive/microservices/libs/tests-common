﻿using System;

namespace ProSkive.Lib.TestsCommon
{
    public static class TestEnv
    {
        public static string Get()
        {
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            if (env == null || !env.Equals("CI"))
                env = "Test";
            return env;
        }
    }
}