﻿using Flurl.Http;
using Flurl.Http.Configuration;
using Microsoft.AspNetCore.TestHost;

namespace ProSkive.Lib.TestsCommon.Http
{
    public static class FlurlTestHelper
    {
        private static IHttpClientFactory _oldFactory;
        
        public static void UseInMemoryServer(TestServer testServer)
        {
            // Save old Client Factory
            _oldFactory = FlurlHttp.GlobalSettings.HttpClientFactory;
            
            // Set In-memory-client-factory
            FlurlHttp.Configure(settings =>
            {
                settings.HttpClientFactory = new FlurlTestHttpClientFactory(testServer);
            });
        }

        public static void ResetClientFactory()
        {
            // Reset to old
            if (!(_oldFactory is null))
            {
                FlurlHttp.Configure(settings => { settings.HttpClientFactory = _oldFactory; });
                _oldFactory = null;
            }
        }
    }
}