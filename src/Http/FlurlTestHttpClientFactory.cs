﻿using System.Net.Http;
using Flurl.Http.Configuration;
using Microsoft.AspNetCore.TestHost;

namespace ProSkive.Lib.TestsCommon.Http
{
    public class FlurlTestHttpClientFactory : DefaultHttpClientFactory
    {
        private readonly TestServer testServer;

        public FlurlTestHttpClientFactory(TestServer testServer)
        {
            this.testServer = testServer;
        }
        
        public override HttpMessageHandler CreateMessageHandler()
        {
            return testServer.CreateHandler();
        }
    }
}