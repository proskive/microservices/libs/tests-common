﻿using Microsoft.AspNetCore.Authentication;
using ProSkive.Lib.TestsCommon.Auth.FakeJwt.Events;

namespace ProSkive.Lib.TestsCommon.Auth.FakeJwt
{
    public class FakeJwtBearerOptions : AuthenticationSchemeOptions
    {
        public new FakeJwtEvents Events
        {
            get => (FakeJwtEvents) base.Events;
            set => base.Events = value;
        }
    }
}