﻿using System;
using Microsoft.AspNetCore.Authentication;

namespace ProSkive.Lib.TestsCommon.Auth.FakeJwt
{
    public static class FakeJwtBearerExtensions
    {
        public static AuthenticationBuilder AddFakeJwtBearer(this AuthenticationBuilder builder)
        {
            return AddFakeJwtBearer(builder, _ => { });
        }
        
        public static AuthenticationBuilder AddFakeJwtBearer(this AuthenticationBuilder builder, Action<FakeJwtBearerOptions> options)
        {
            return builder.AddScheme<FakeJwtBearerOptions, FakeJwtBearerHandler>(FakeJwtBearerDefaults.AuthenticationScheme, options);
        }  
    }
}