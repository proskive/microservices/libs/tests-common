﻿using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ProSkive.Lib.TestsCommon.Auth.FakeJwt.Events;

namespace ProSkive.Lib.TestsCommon.Auth.FakeJwt
{
    public class FakeJwtBearerHandler : AuthenticationHandler<FakeJwtBearerOptions>
    {

        public new FakeJwtEvents Events
        {
            get => (FakeJwtEvents) base.Events;
            set => base.Events = value;
        }

        protected override Task<object> CreateEventsAsync()
        {
            return Task.FromResult<object>(new FakeJwtEvents());
        }

        public FakeJwtBearerHandler(IOptionsMonitor<FakeJwtBearerOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock) 
            : base(options, logger, encoder, clock)
        {
        }

        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            var fakeTokenValidatedContext = new FakeTokenValidatedContext(Context, Scheme, Options);
            fakeTokenValidatedContext.Success();
            return Task.FromResult(fakeTokenValidatedContext.Result);
        }
    }
}