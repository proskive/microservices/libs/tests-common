﻿using System;
using System.Threading.Tasks;

namespace ProSkive.Lib.TestsCommon.Auth.FakeJwt.Events
{
    public class FakeJwtEvents
    {
        public Func<FakeJwtBearerChallengeContext, Task> OnChallenge { get; set; } = context => Task.CompletedTask;

        public virtual Task Challenge(FakeJwtBearerChallengeContext context) => OnChallenge(context);
    }
}