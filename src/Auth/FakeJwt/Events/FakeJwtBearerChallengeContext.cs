﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;

namespace ProSkive.Lib.TestsCommon.Auth.FakeJwt.Events
{
    public class FakeJwtBearerChallengeContext : PropertiesContext<FakeJwtBearerOptions>
    {
        public bool Handled { get; set; }
        
        public FakeJwtBearerChallengeContext(HttpContext context, AuthenticationScheme scheme, FakeJwtBearerOptions options, AuthenticationProperties properties) 
            : base(context, scheme, options, properties)
        {
        }

        public void HandleResponse()
        {
            Handled = true;  
        } 
    }
}