﻿using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;

namespace ProSkive.Lib.TestsCommon.Auth.FakeJwt.Events
{
    public class FakeTokenValidatedContext : ResultContext<FakeJwtBearerOptions>
    {
        public ClaimsIdentity Identity { get; } = new ClaimsIdentity(new Claim[]
        {
            new Claim("roles", "user"), 
        }, FakeJwtBearerDefaults.AuthenticationScheme);
        
        public FakeTokenValidatedContext(HttpContext context, AuthenticationScheme scheme, FakeJwtBearerOptions options) : base(context, scheme, options)
        {
            Principal = new ClaimsPrincipal(Identity);
        }
    }
}