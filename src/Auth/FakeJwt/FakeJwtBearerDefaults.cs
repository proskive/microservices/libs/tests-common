﻿namespace ProSkive.Lib.TestsCommon.Auth.FakeJwt
{
    public static class FakeJwtBearerDefaults
    {
        public const string AuthenticationScheme = "FakeJwt";
    }
}